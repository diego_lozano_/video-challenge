// Video Clip model to reuse on app
export class VideoClip {
  constructor(public name: string, public timeStart: string, public timeEnd: string, public tags: string[], public mainVideoUrl: string, public isMainVideo: boolean) {}
}
