
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoMainComponent } from './video-main.component';

describe('VideoMainComponent', () => {
  let component: VideoMainComponent;
  let fixture: ComponentFixture<VideoMainComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoMainComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VideoMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
