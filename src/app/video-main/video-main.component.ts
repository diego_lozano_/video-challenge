import { Component, AfterViewChecked, AfterViewInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { InputVideoDialogComponent } from '../input-video-dialog/input-video-dialog.component';
import { AddClipDialogComponent } from '../add-clip-dialog/add-clip-dialog.component';
import { VideoClipService } from '../services/videoclip.service';

@Component({
  selector: 'video-main',
  templateUrl: './video-main.component.html',
  styleUrls: ['./video-main.component.css']
})
export class VideoMainComponent implements AfterViewInit {

  public fullvideo: string  = null;
  videoLength: number = null;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,public dialog: MatDialog, private videoClipService:VideoClipService) {}


  // Opens dialog to input main video url
  openDialog(): void {
    let dialogRef = this.dialog.open(InputVideoDialogComponent, {
      width: '500px',
      height: '300px',
      id: 'inputURLDialog',
      disableClose: true,
      data: {}
    });
    var that = this;
    dialogRef.afterClosed().subscribe(result => {
      that.fullvideo = result;
    });
  }

  ngAfterViewInit() {
    // Subscribe to get main video length
    this.videoClipService.videoLength.subscribe( res => {
      if(res>0) {
        this.videoLength = res;
      }
    });

    // Opens dialog when there is no main video src
    if(!this.fullvideo) {
      this.openDialog();
    }
  }

  // Opens dialog to add a clip from main video
  openAddClipDialog(): void {
    let dialogRef = this.dialog.open(AddClipDialogComponent, {
      width: '800px',
      height: '650px',
      id: 'addClipDialog',
      disableClose: true,
      data: {videoUrl: this.fullvideo, mainVideoLength: this.videoLength}
    });

    //After close dialog adds clip to clips list and sends to update on components
    dialogRef.afterClosed().subscribe(result => {
        if(result.name) {
          this.videoClipService.addClip(result);
        }
    });
  }


  }


