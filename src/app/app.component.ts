import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { InputVideoDialogComponent } from './input-video-dialog/input-video-dialog.component';
import { VideoClipService } from './services/videoclip.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'},
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private videoClipService: VideoClipService) {}

  hotkeys(event){
    if (event.keyCode == 37){
       console.log('prev');
       this.videoClipService.playPrevClip();
    }
    if (event.keyCode == 39) {
      console.log('next')
      this.videoClipService.playNextClip();
    }
  }




}

