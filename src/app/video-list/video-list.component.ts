import { Component, OnInit, Input } from '@angular/core';
import { VideoClip } from '../models/videoclip.model';
import { VideoClipService } from '../services/videoclip.service';
import { MatDialog } from '@angular/material';
import { AddClipDialogComponent } from '../add-clip-dialog/add-clip-dialog.component';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {

  videoClipList: VideoClip[] = [];
  videoLength: number;

  currentClipIndex: number;

  search: string;

  @Input() editable: boolean;
  @Input() mainVideo: string;

  constructor(private videoClipService: VideoClipService,public dialog: MatDialog) { }

  ngOnInit() {
    // Subscribe to get clip list from service
    this.videoClipService.currentMessage.subscribe(clipList => this.videoClipList = clipList);

    // If clip list is empty adds the main video as the 1st element
    if(this.videoClipList.length == 0) {
      this.videoClipService.addClip(new VideoClip('Main Video','','',[],this.mainVideo,true));
    }

    // Subscribe to get main video length
    this.videoClipService.videoLength.subscribe( res => {
      if(res>0) {
        this.videoLength = res;
      }
    });

    // Subscribe on clip play event to get current playing clip
    this.videoClipService.currentClip.subscribe(clip => {
      if (clip ) {
        this.currentClipIndex = clip['index'];
      }
    });



    // Sends to play 1st video on playlist
    this.videoClipService.changeVideoToPlay(this.mainVideo);
    this.videoClipService.setCurrentClip(this.videoClipList[0],0);
  }

  // Opens dialog to edit clip
  editClip(clip: VideoClip,index:number) {
    let dialogRef = this.dialog.open(AddClipDialogComponent, {
      width: '800px',
      height: '800px',
      id: 'addClipDialog',
      disableClose: true,
      data: {videoUrl: clip.mainVideoUrl, isEdit: true, clip:clip, mainVideoLength:this.videoLength}
    });

    // After close dialog replaces the edited clip
    dialogRef.afterClosed().subscribe(result => {
        this.videoClipService.replaceClipFromList(result,index);
        this.playVideo(clip,index);
    });
  }

  // Removes clip from clips list
  removeClip(index:number) {
    this.videoClipService.removeClipFromList(index);
  }


  // Sends signal to service to play selected clip on player component
  playVideo(clip: VideoClip,index:number) {
    let videoUrl = '';
    if(clip.isMainVideo) {
      videoUrl = clip.mainVideoUrl;
    } else {
      // Generates fragment url to only play selected time from main clip
      videoUrl = clip.mainVideoUrl+'#t='+clip.timeStart+','+clip.timeEnd;
    }
    this.videoClipService.changeVideoToPlay(videoUrl);
    this.videoClipService.setCurrentClip(clip,index);
  }

}
