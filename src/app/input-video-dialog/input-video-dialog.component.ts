import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VideoMainComponent } from '../video-main/video-main.component';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-input-video-dialog',
  templateUrl: './input-video-dialog.component.html',
  styleUrls: ['./input-video-dialog.component.css']
})
export class InputVideoDialogComponent implements OnInit {

  // Sets default video url, for testing purposes...
  public videoUrl: String = '';
  public urlFormControl = new FormControl('',[
    Validators.required
  ]);

  constructor(
    public dialogRef: MatDialogRef<VideoMainComponent>) { }

  ngOnInit() {
  }

  onSelectURL() {
    this.dialogRef.close(this.videoUrl);
  }

}
