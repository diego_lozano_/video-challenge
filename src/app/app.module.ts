import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VideoMainComponent } from './video-main/video-main.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatDialogModule, MatInputModule, MatCardModule, MatChipsModule, MatSliderModule, MatGridListModule, MatTooltipModule, MatProgressSpinnerModule } from '@angular/material';
import { VideoListComponent } from './video-list/video-list.component';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { InputVideoDialogComponent } from './input-video-dialog/input-video-dialog.component';
import { AddClipDialogComponent } from './add-clip-dialog/add-clip-dialog.component';
import { VideoClipService } from './services/videoclip.service';
import { SearchPipe } from './pipes/search.pipe';

@NgModule({
  declarations: [
    AppComponent,
    VideoMainComponent,
    VideoListComponent,
    VideoPlayerComponent,
    InputVideoDialogComponent,
    AddClipDialogComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    MatInputModule,
    MatCardModule,
    ReactiveFormsModule,
    MatChipsModule,
    MatSliderModule,
    MatGridListModule,
    MatTooltipModule,
    MatProgressSpinnerModule

  ],
  providers: [VideoClipService],
  bootstrap: [AppComponent],
  entryComponents: [InputVideoDialogComponent,AddClipDialogComponent]
})
export class AppModule { }
