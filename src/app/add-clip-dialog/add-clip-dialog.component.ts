import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VideoClip } from '../models/videoclip.model';
import { FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-add-clip-dialog',
  templateUrl: './add-clip-dialog.component.html',
  styleUrls: ['./add-clip-dialog.component.css']
})
export class AddClipDialogComponent implements OnInit {
  mainVideoUrl: string;
  name: string;
  start: string;
  end: string;
  tags: string[] = [];
  tag: string;
  isEdit: boolean;
  mainVideoLength: number;

  //Form controls
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  startTimeFormControl = new FormControl('', [
    Validators.required,
    this.checkTime(null,true)
  ]);
  endTimeFormControl = new FormControl('', [
    Validators.required,
    this.checkTime(this.startTimeFormControl,false)
  ]);

  constructor(public dialogRef: MatDialogRef<AddClipDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

    this.mainVideoUrl = this.data.videoUrl;
    this.mainVideoLength = this.data.mainVideoLength;

    if (this.data.isEdit) {
      this.isEdit = true;
      let clipEdit = <VideoClip>this.data.clip;
      this.name = clipEdit.name;
      this.start = clipEdit.timeStart;
      this.end = clipEdit.timeEnd;
      this.tags = clipEdit.tags;
    }
  }

  // Creates a new VideoClip instance after closing dialog
  saveClip() {
    this.dialogRef.close(new VideoClip(this.name, this.start, this.end, this.tags, this.mainVideoUrl, false));
  }

  addTag() {
    this.tags.push(this.tag);
    this.tag = '';
  }


  removeTag(tag: string) {
    var that = this;
    this.tags.forEach(function (item, index) {
      if (tag == item) {
        that.tags.splice(index, 1);
      }
    });
  }

  // Validator to check value from start and end time input at creating clip
  // Checks value with other control end vs start to check valid time range
  checkTime(otherControl:AbstractControl,isStart:boolean): ValidatorFn {
    var that = this;
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if(!control.value) {
        return null
      }
      if (that.asDecimal(control.value) > that.mainVideoLength) {
        return { 'exceedsMain': true }
      }
      if (!isStart && that.asDecimal(control.value) < that.asDecimal(otherControl.value)) {
        return { 'timeRange': true }
      }
      return null;
    }
  }

  // Parses input in time format hh:mm:ss to decimal xx.yy
  asDecimal(time: string) {
    if (time) {
      var hoursMinutes = time.split(':');
      var result = parseInt(hoursMinutes[0]) * 3600 + parseInt(hoursMinutes[1]) * 60 + parseInt(hoursMinutes[2]) * 1;
      return result;
    } else {
      return 0;
    }
  }


}
