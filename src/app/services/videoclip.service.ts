import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { VideoClip } from '../models/videoclip.model';

@Injectable()
export class VideoClipService {
  private clipList: VideoClip[] = [];

  private messageSource = new BehaviorSubject([]);
  private videoSource = new BehaviorSubject('');
  private clipSource = new BehaviorSubject({});
  private mainVideoLength = new BehaviorSubject(0);
  private clipChange = new BehaviorSubject('');

  currentMessage = this.messageSource.asObservable();
  currentVideo = this.videoSource.asObservable();
  currentClip = this.clipSource.asObservable();
  videoLength = this.mainVideoLength.asObservable();
  changeTrack = this.clipChange.asObservable();

  constructor() { }

  addClip(videoclip: VideoClip) {
    this.clipList.push(videoclip);
    this.messageSource.next(this.clipList);
  }

  changeVideoToPlay(videoUrl:string) {
    this.videoSource.next(videoUrl);
  }

  setCurrentClip(videoclip: VideoClip,index:number) {
    this.clipSource.next({clip:videoclip,index:index});
  }

  setMainVideoLength(length:number) {
    this.mainVideoLength.next(length);
  }

  replaceClipFromList(videoClip:VideoClip,index:number) {
    this.clipList[index] = videoClip;
    this.messageSource.next(this.clipList);
  }

  removeClipFromList(index:number) {
    this.clipList.splice(index,1);
    this.messageSource.next(this.clipList);
  }

  playNextClip() {
    this.clipChange.next('next');
  }

  playPrevClip() {
    this.clipChange.next('prev');
  }

}
