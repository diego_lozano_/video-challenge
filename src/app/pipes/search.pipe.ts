import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchTags'
})
export class SearchPipe implements PipeTransform {
  transform(value: any, input: string) {
    if (input) {
      let filteredItems: string[] = [];
      value.forEach(element => {
        if (element.tags.includes(input)) {
          filteredItems.push(element);
        }
      });
      return filteredItems;

    }
    return value;
  }
}
