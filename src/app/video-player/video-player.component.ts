import { Component, OnInit, Input } from '@angular/core';
import { VideoClipService } from '../services/videoclip.service';
import { VideoClip } from '../models/videoclip.model';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnInit {

  @Input() video: string;

  videoTag = <HTMLVideoElement>document.getElementById("video");

  clip: VideoClip;
  selectedIndex: number;

  clipList: VideoClip[] = [];

  isLoading:boolean = false;

  // Buttons
  playButton = document.getElementById("play-pause");
  muteButton = document.getElementById("mute");
  fullScreenButton = document.getElementById("full-screen");

  // Sliders
  seekBar: any = document.getElementById("seek-bar");
  volumeBar: any = document.getElementById("volume-bar");

  // Counters of playing time and total time
  currentTime: string;
  totalTime: string;

  constructor(private videoClipService: VideoClipService) {

  }

  ngOnInit() {
    var that = this;

    // Subscrive on clip play event to change video src
    this.videoClipService.currentVideo.subscribe(url => {
      if (url.length > 0) {
        that.changeVideo(url);
      }
    });

    // Subscribe on clip play event to get clip object
    this.videoClipService.currentClip.subscribe(clip => {
      let currentClip = <VideoClip>clip['clip'];
      if (currentClip && currentClip.name) {
        that.clip = currentClip;
        that.selectedIndex = clip['index'];
        this.seekBar = document.getElementById("seek-bar");
      }
    });

    // Subscribe to changes on clip list
    this.videoClipService.currentMessage.subscribe(clipList => {
      this.clipList = clipList;
    });

    this.videoClipService.changeTrack.subscribe(res => {
      if (res.localeCompare('next') == 0) {
        this.playNext();
      } else if(res.localeCompare('prev') == 0) {
        this.playPrev();
      }
    });

    // Player
    this.videoTag = <HTMLVideoElement>document.getElementById("videoPlayer");

    // Buttons
    this.playButton = document.getElementById("play-pause");
    this.muteButton = document.getElementById("mute");
    this.fullScreenButton = document.getElementById("full-screen");

    // Sliders
    this.seekBar = document.getElementById("seek-bar");
    this.volumeBar = document.getElementById("volume-bar");

    // Event to switch between play and pause
    this.playButton.addEventListener("click", function () {
      if(that.videoTag.currentTime < that.videoTag.duration) {
        
      if (that.videoTag.paused == true) {
          that.videoTag.play();
          that.playButton.innerHTML = "Pause";
        
      } else {
        that.videoTag.pause();
        that.playButton.innerHTML = "Play";
      }
    }
    });

    // Event to mute clip/video
    this.muteButton.addEventListener("click", function () {
      if (that.videoTag.muted == false) {
        that.videoTag.muted = true;
        that.muteButton.innerHTML = "Unmute";
      } else {
        that.videoTag.muted = false;
        that.muteButton.innerHTML = "Mute";
      }
    });

    // Event to toggle full screen
    this.fullScreenButton.addEventListener("click", function () {
      if (that.videoTag.requestFullscreen) {
        that.videoTag.requestFullscreen();
      } else if (that.videoTag.webkitRequestFullscreen) {
        that.videoTag.webkitRequestFullscreen(); // Chrome and Safari
      }
    });

    // Event to update timeline
    this.videoTag.addEventListener("timeupdate", function () {
      // Calculate the slider value
      that.currentTime = that.timeConvert(that.videoTag.currentTime);
      if (that.clip && !that.clip.isMainVideo) {
        that.totalTime = that.clip.timeEnd;
      } else {
        that.totalTime = that.timeConvert(that.videoTag.duration);
      }

      var value = (100 / that.videoTag.duration) * that.videoTag.currentTime;

      // Update the slider value
      that.seekBar.value = value;

      //If the video ends, plays next after 3 sec wait, while shows loading spinner
      if(Math.floor(that.videoTag.currentTime)==Math.floor(that.asDecimal(that.totalTime)) && that.clipList.length>0 && that.selectedIndex != that.clipList.length-1) {
        that.isLoading = true;
        setTimeout(function() {
          that.isLoading = false;
          that.playNext();
        }, 3000);
      }
    });

    this.volumeBar.addEventListener("change", function () {
      // Update the video volume
      that.videoTag.volume = that.volumeBar.value;
    });

    // Gets video duration before load
    this.videoTag.addEventListener('loadedmetadata', function () {
      var duration = that.videoTag.duration;
      if (that.clip && that.clip.isMainVideo) {
        that.videoClipService.setMainVideoLength(duration);
      }
    });

  }

  // Calls if player has error loading url / url is incorrect
  onError() {
    alert('Error loading video, reload the page and use a valid video url');
    window.location.reload();
  }

  playNext() {
    let clipsNumber = this.clipList.length;
    if(this.selectedIndex < clipsNumber-1) {
      this.playVideo(this.clipList[this.selectedIndex+1],this.selectedIndex+1);
    }
  }

  playPrev() {
    let clipsNumber = this.clipList.length;
    if(this.selectedIndex-1 >= 0 && this.selectedIndex-1 < clipsNumber) {
      this.playVideo(this.clipList[this.selectedIndex-1],this.selectedIndex-1);
    }
  }

  timeConvert(num) {
    var hours = Math.floor(num / 60);
    var minutes = num % 60;
    return '00:'+Math.floor(hours) + ":" + Math.floor(minutes);
  }

  // Loads a new source in the player
  changeVideo(url: string) {
    this.video = url;
    let video = <HTMLVideoElement>document.getElementById('videoPlayer');
    video.pause();
    var source = document.createElement('source');
    source.setAttribute('src', url);
    video.appendChild(source);
    video.load();
    video.play();
  }

  // Method to download file, using media fragment query but doesnt work as expected :(
  downloadFile() {
    var link = document.createElement("a");
    link.download = this.clip.name;
    link.href = this.video.replace("#", "?");
    link.target = "_blank";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  // Calculates the left margin to attach markers on timeline
  getItemStyle(clip: VideoClip) {
    let video = <HTMLVideoElement>document.getElementById('videoPlayer');
    let widthPX = video.offsetWidth
    let clipStartNumber = this.asDecimal(clip.timeStart);
    let offsetPX = (clipStartNumber * widthPX) / this.videoTag.duration;
    return Math.floor(offsetPX - 7);
  }

  // Converts string time to decimal
  asDecimal(time: string) {
    if (time) {
      var hoursMinutes = time.split(':');
      var result = parseInt(hoursMinutes[0]) * 3600 + parseInt(hoursMinutes[1]) * 60 + parseInt(hoursMinutes[2]) * 1;
      return result;
    } else {
      return 0;
    }
  }

  getClipTooltip(clip: VideoClip) {
    return clip.name + ' | ' + clip.timeStart + ' - ' + clip.timeEnd;
  }

  // Plays the selected clip, used on timeline markers
  playVideo(clip: VideoClip,index:number) {
    let videoUrl = '';
    if(clip.isMainVideo) {
      videoUrl = clip.mainVideoUrl;
    } else {
      // Generates fragment url to only play selected time from main clip
      videoUrl = clip.mainVideoUrl+'#t='+clip.timeStart+','+clip.timeEnd;
    }
    this.videoClipService.changeVideoToPlay(videoUrl);
    this.videoClipService.setCurrentClip(clip,index);
  }



}
