# VideoChallenge

Video Challenge Project using [Angular 6](https://angular.io/) and [html5 media fragments](https://www.w3.org/TR/media-frags/)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Requirements

- Node.js version 8.x or greater and npm version 5.x or greater
- Angular 6
- Internet connection

## Run Development server

1. Install Angular Cli by npm `npm install -g @angular/cli`
2. Clone Repo
3. Go inside cloned repo directory
4. Run `npm install` to install dependencies. 
6. Run `ng serve` for a dev server. 
6. Navigate to `http://localhost:4200/`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### MADE BY Diego Lozano
